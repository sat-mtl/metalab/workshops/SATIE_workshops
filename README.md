# Atelier de SATIE pour Eastern Bloc

## Prérequis
- Un laptop (Linux, OSX, Windows)
- Un casque audio
- Les logiciels suivants installés
  - [SATIE](https://gitlab.com/sat-metalab/SATIE)
    - qui implique [SuperCollider](https://supercollider.github.io/) mais idéalement installé selon [ces instructions](https://gitlab.com/sat-metalab/SATIE/-/blob/master/INSTALL-SATIE.md)
  - [Jupyter Notebook](https://jupyter.org/)
    - avec un environnement Python configuré correctement
    - [pyliblo3](https://pypi.org/project/pyliblo3/)

Il est possible d'utiliser un logiciel de votre choix capable d'envoyer et recevoir des messages OSC au lieux de Jupyter Notebook et Python.

# Le déroulement du workshop
# Deep Note clone
### Théorie
https://musicinformationretrieval.com/thx_logo_theme.html
## Premier contact avec SATIE
- comprendre la base de communication OSC avec SATIE
- création d'un première instance sonore
- contrôle de ses propriétés
- destruction de la première instance
## Exploring the sounds
Explorer quelques possibilités et propriétés des sources sonores de SATIE:
- `MonoIn` - lire une source qui vient par jack (hardware ou un autre software)
- `zkarpluck1` - corde pincée
- `SawMoog` - un synthé classique qui joue tout seul
- `Tubie` - comme avant
- `RezonPing` - comme avant
- `saw` - le son que nous allons utiliser tout au long le l'atelier
- `default` - pour testing
## Instantier plusieurs sons
## Scene et Nodes
Expliquer le concept et apprendre à interptéter l'API OSC de SATIE
## Instantier et contrôler
### `update` vs `set`
## Spatialisation
exploration des types de spatialiseurs
### VBAP + panning
on explique les spatialiseurs classiques de sources mono
### HOA
- introduction à B-Format
- explication comment B-Format est traité dans SATIE
### Spatialiseurs - under the hood
Comment configurer les différents spatialiseurs dans SATIE et comme s'en servir
## PostProcess
expliquer c'est quoi. à qua ça sert et que c'est nécessaire pour HOA
## Pratique
- Explorer `saw` avec `stereoListener` et `HOABinaural`
- démo de `saw` sur `cube`
- commencer récréation de Deep Note
## Groupes
Lorsqu'on veut contrôler une propriété sur plusieurs instances
## Effets
## Effects
- effect busses
- comment configurer SATIE et les sources pour utiliser effect busses
## Pratique
- récréation de THX avec reverb
## Faire outrement: process
- expliquer c'est quoi
- exemple de Deep Note process?
## SATIE - utilisation avancée:
- mappers
## SATIE - Anatomie
- Comment ça marche under the hood
- comment créer ses propres sources/effects/spatialiseurs
## Conclusion
