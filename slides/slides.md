---
title: "Atelier SATIE - Découverte"
author: Michał Seta, Thomas Piquet
date: Montréal, 25 Février 2023
theme: night
format: revealjs
...


Présentations
=============

Formateurs
----------

mseta@sat.qc.ca

tpiquet@sat.qc.ca

\[SAT\] Société des arts technologiques
=======================================

-   Organisation à But Non Lucratif depuis 1996
-   Expertise Art & Immersion & Téléprésence

Diffusion + Formation + Création + R&D
--------------------------------------

![](./img/SAT-batiment.png)

\[SAT\] Metalab
===============

Laboratoire de recherche et développement de la SAT

<div align="left">
-   **Thèmes de recherche** : téléprésence, immersion, mapping vidéo,
    son spatialisé, interaction
-   **Mission** : créer des outils pour faciliter l\'accès aux nouvelles
    technologies

Valorisation de la recherche:
-----------------------------

<div align="left">
-   Transfert technologique
-   Communications scientifiques
-   Animation d'ateliers centrés sur l'immersion 
-   Logiciels libres (GPL, LGPL)
-   <https://sat.qc.ca/recherche>

Plan de la journée
==================

Objectif
--------
Découvrir SATIE, ses possibilité et apprendre les fonctionnalité de base

Ce qu'on va réaliser
--------------------
Deep Note

![](./img/thx.jpeg)

Plan général (1/2)
------------------

<div align="left">
- Context de la spatialisation sonnore
- Démonstration de possibilités offertent par SATIE
- SuperCollider - le moteur de SATIE
- Le protocole OSC - l'interface de SATIE
- Ossia score - un conducteur de SATIE

Plan général (2/2)
------------------
<div align="left">
- Anatomie de la deep note
- Premier pas avec SATIE
- Il est temps de pratiquer !

Tour de table
=============

<div align="left">
- Qui êtes vous ?
- Ce que vous attendez de cet atelier
- Votre expérience avec l'immersion / spatialisation sonnore
- Vos outils de créations préféré

Context
=======

SATosphère
----------

![](./img/sato-speakers.png){width="50%" height="50%"}

-   157 hauts-parleurs + 4 subs (31 canaux)
-   Système actif basé sur un serveur audio digital D-Mitri(Meyer)
-   18m de diamètre et 11,5 à 13m de hauteur (plus de 300 personnes)
-   8 projecteurs vidéo
-   Résolution en entrée de 4096x4096 pixels
-   Écran modulaire : 180 ◦ , 210 ◦ et 230 ◦ en vertical par 360 ◦

Spatialisation sonore surround
------------------------------

![](./img/speakers-surround-head-3d.png)

Spatialisation sonore multi-directionnel
----------------------------------------
Avec les Audiodices

![](./img/audiodice_cirmmt.jpg)


Le principal intéressé
======================

![](./img/satie-white.png){width="50%" height="50%"}

Spatial Audio Toolkit for Immersive Environments

(depuis 2015)

<https://gitlab.com/sat-mtl/tools/satie/satie>

Spatialisation avec SATIE
-------------------------

<div align="left">
-   synchronisé avec les moteurs 3D (OSC)
-   multitude de haut-parleurs
-   VBAP / ambisonics
-   rendus simultanés
-   robuste et performant

Démos SATIE
-----------
[Démo reel](https://vimeo.com/616508216)

<div align="left">
-   [Demo Blender](https://vimeo.com/398346954)
-   Demo Ableton Live
-   SATIE avec Unity
    - [OSM](https://vimeo.com/774457566)
    - [BallJam](https://vimeo.com/234778380)
    - [Aqua Khoria](https://vimeo.com/182367434)
-   SATIE avec Godot
    - [Fadeferra](https://libre.video/videos/watch/d2cbe43d-d0ee-4e29-b263-9357b13c17ce)
    - [Mimoidalaube](https://vimeo.com/483783290)



Supercollider
=============
Le moteur de SATIE

<div align="left">
- Un serveur audio temps réel
    - Synthétiseurs
    - Effets
- Un language de programmation
- Un éditeur de code

Client - Serveur ?
------------------

![](./img/supercollider.png)

Le protocole OSC
================
L'interface de SATIE

- Open Sound Control
- Developpé à l'université de Berkley, début 2000
- Comparable au MIDI
    - Protocol réseaux
    - Architecture client / serveur

Envoyer des messages OSC
-----------------------

<div align="left">
1. Connexion a un serveur OSC
    - Adresse IP
    - Numero de port
2. Envois d'une commande
    - Destination de la commande
    - Valeur

Exemple de commandes:
```
/satie/source/set myCuteSynth aziDeg -90
/satie/source/set myCuteSynth aziDeg 90 freq 200
```

Ossia Score
===========
Un conducteur de SATIE
https://ossia.io/


Atelier
=======

Déroulement
-----------

<div align="left">
- Prise en main de SATIE et Ossia
- Travail individuel / petit groupe 
    - au casque
- Tous le monde en même temps !
    - dans les 8 haut-parleurs

Matériaux nécessaires
---------------------

<div align="left">
- Un ordinateur
- Un casque
- Une connexion au reseau local

Logiciels nécessaires
---------------------

<div align="left">
- SATIE
- Ossia score

Deep Note - Anatomie du son
===========================

![](./img/deep_note.jpeg)

<https://musicinformationretrieval.com/thx_logo_theme.html>