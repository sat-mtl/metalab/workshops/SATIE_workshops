#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created between Feb 15-27 2021

@author: Marc Lavallée <mlavallee@sat.qc.ca>

Basic SatieOSC class to communicate with SATIE using OSC.

It tries to use pyliblo3 if installed on the system,
else it tries to use python-osc for one way communication.

"""

import sys
import time
import json
from importlib import import_module

import logging
logging.getLogger("matplotlib").setLevel(logging.WARNING)
logging.basicConfig(level=logging.WARNING)


osc_lib_names = ['liblo', 'pythonosc']
for osc_lib_name in osc_lib_names:
    try:
        import_module(osc_lib_name)
        print("Found OSC module:", osc_lib_name)
        break
    except:
        print(sys.exc_info())

if osc_lib_name == 'pythonosc':
    from pythonosc import udp_client

    class SatieOSC():
        """
        Create a OSC link with SATIE
        """
        def __init__(self, _port=18060, satie_host="localhost", satie_port=18032):
            self.logger = logging.getLogger(self.__class__.__name__)
            self._client = udp_client.SimpleUDPClient(satie_host, satie_port)

        def send(self, path, *args):
            self._client.send_message(path, args)


if osc_lib_name == 'liblo':
    import liblo

    class SatieOSC(liblo.ServerThread):
        """
        Create a OSC link with SATIE, querying for info on plugins at initialization
        """

        def __init__(self, _port=18060, satie_host="localhost", satie_port=18032):
            self._port = _port
            self.satie_host = satie_host
            self.satie_port = satie_port
            self.plugins = {}
            self.logger = logging.getLogger(self.__class__.__name__)
            super().__init__()

            try:
                self.start()
                self._get_plugins()
            except liblo.ServerError as e:
                self.logger.error("Could not start SatieOSC because:%s", e)

        def __del__(self):
            self.logger.debug("delete")

        def send(self, message, *args):
            liblo.send((self.satie_host, self.satie_port),
                       liblo.Message(message, *args))

        def _get_plugins(self):
            self.send("/satie/plugins")
            self.logger.debug("asked about plugins")
            # wait for all results
            time.sleep(0.25)

        @liblo.make_method(None, None)
        def _parse_plugins(self, path, json_results):
            """parsing json info about plugins"""
            self.logger.debug("parsing json info about plugins")
            if type(json_results) != list:
                return

            for json_result in json_results:
                result = json.loads(json_result)
                if type(result) != dict:
                    continue

                if path == "/plugins":
                    for (category, plugins) in result.items():
                        for (plugin, details) in plugins.items():
                            self.plugins[plugin] = details
                            self.plugins[plugin]["category"] = category
                            self.send("/satie/plugindetails", plugin)

                elif path == "/arguments":
                    for (plugin, details) in result.items():
                        self.plugins[plugin]["arguments"] = details["arguments"]


def midiToFreq(note):
    a = 440
    return (a / 32) * (2 ** ((note - 9) / 12))
